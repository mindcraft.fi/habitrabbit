module.exports = {
    entry: "./src/index.tsx",
    output: {
        filename: "bundle.js",
        path: __dirname + "/dist",
	publicPath: './'
    },

    devtool: "source-map",

    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"]
    },

    module: {
        rules: [
{ 
    test: /\.css$/,  
    use: [ 
	  "style-loader",
	  "typings-for-css-modules-loader?modules&namedExport"
	   ]
},
{ 
    test: /\.tsx?$/, 
    loader: "awesome-typescript-loader" 
},
// All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
{ enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
        ]
    }/*,

    externals: {
        "react": "React",
        "react-dom": "ReactDOM"
	}*/
};