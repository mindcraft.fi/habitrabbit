import * as React from 'react';
//import * as ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { createStore, applyMiddleware, combineReducers } from 'redux';
import { Provider } from 'react-redux';

import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native
import { PersistGate } from 'redux-persist/integration/react'

import { rootReducer, initialState } from './reducers/index';

import HabitCalendarSceneContainer from './containers/HabitCalendarSceneContainer';

const persistConfig = {
    key: 'root',
    storage: storage,
    blacklist: ['habitCalendar']
}

const persistedReducer = persistReducer(persistConfig, rootReducer);

// TODO const store = createStoreWithMiddleware(rootReducer);
// TODO apply sagas middleware
//const store = createStore(rootReducer, initialState);

const store = createStore(persistedReducer)
const persistor = persistStore(store)


export default class HabitRabbitApp extends React.Component {
    render() {
	return (
	<Provider store={store}>
	  <PersistGate loading={null} persistor={persistor}>
	    <Router basename='/HabitRabbit/'>
	      <Route exact path='/' component={HabitCalendarSceneContainer} />
	    </Router>
	  </PersistGate>
	</Provider>
	)
    }
}
