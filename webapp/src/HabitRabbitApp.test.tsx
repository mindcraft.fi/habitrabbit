import * as React from 'react';
import * as ReactDOM from 'react-dom';
import HabitRabbitApp from './HabitRabbitApp';

describe('HabitRabbitApp', () => {
    test('renders without crashing', () => {
	const rootDiv = document.createElement('root');
	ReactDOM.render(<HabitRabbitApp />, rootDiv);
	ReactDOM.unmountComponentAtNode(rootDiv);
    });
});
