import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import HabitCalendarSceneComponent from '../components/HabitCalendarSceneComponent';
import { AddHabitEntryAction } from '../actions/HabitEntriesActions';
import { SetDateRangeAction } from '../actions/HabitCalendarActions';
import { HabitEntryBase } from '../utils/HabitCalendar';
import { selectors, HabitRabbitState } from '../reducers/index';

function mapStateToProps(state: HabitRabbitState) {
    console.log(state);
    return {
	startDate: selectors.calendar.startDate(state),
	endDate: selectors.calendar.endDate(state),	
	habitCalendar: selectors.habitCalendar(state)
    }
}

function mapDispatchToProps(dispatch: Dispatch<AddHabitEntryAction | SetDateRangeAction>) {
    return {
	addHabitEntry: (habitEntry: HabitEntryBase) => {
	    dispatch(AddHabitEntryAction.create(habitEntry)); },
	setDateRange: (startDate: Date, endDate: Date) => {
	    dispatch(SetDateRangeAction.create(startDate, endDate)); }
    }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HabitCalendarSceneComponent);
