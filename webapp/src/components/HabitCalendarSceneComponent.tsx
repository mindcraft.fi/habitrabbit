import * as React from 'react';
import { HabitCalendarView } from './HabitCalendarView';
import HabitCalendar, { HabitEntryBase, millisPerDay } from '../utils/HabitCalendar';
import { AddHabitEntryAction } from '../actions/HabitEntriesActions';
import { SetDateRangeAction } from '../actions/HabitCalendarActions';

//import * as css from './HabitCalendarSceneComponent.css';


const mainContentStyle = {
    position: 'absolute',
    top: '0px',
    bottom: '0px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'stretch',
    alignItems: 'stretch',
    width: '100%',
    maxWidth: '15cm',
    boxShadow: '0px 0px 10px #808080'
} as React.CSSProperties;

const habitCalendarViewStyle = {
    position: 'relative',
    flex: '1 1 auto'
} as React.CSSProperties;

    

interface HabitCalendarSceneComponentProps {
    habitCalendar: HabitCalendar,
    startDate: Date,
    endDate: Date,
    addHabitEntry(habitEntry: HabitEntryBase): void,
    setDateRange(startDate: Date, endDate: Date): void
}

export default class HabitCalendarSceneComponent extends React.Component<HabitCalendarSceneComponentProps,{}> {
    render() {
	
	let weekBefore = new Date(this.props.startDate.getTime() - 7 * millisPerDay);
	let endDate = this.props.endDate;

	return (
		<div style={mainContentStyle}>
		<div style={{position: 'relative', width: '100%', height: '1cm', backgroundColor: 'rgb(32, 160, 192)', color: 'white', display: 'flex', justifyContent: 'center', alignItems: 'center', fontWeight: 'bold'}}>Habit Rabbit</div>
		<button style={{position: 'relative', border: '0px', backgroundColor: '#808080', color: '#ffffff', padding: '2mm', marginTop: '1mm', marginBottom: '1mm'}} onClick={() => this.props.setDateRange(weekBefore,this.props.endDate)}>show more</button>
		<HabitCalendarView style={habitCalendarViewStyle} habitCalendar={this.props.habitCalendar} addHabitEntry={this.props.addHabitEntry} setDateRange={this.props.setDateRange} />
		</div>

	)
    }
}

