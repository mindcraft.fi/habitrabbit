import * as React from 'react';

import { boundMethod } from 'autobind-decorator'

export class View extends React.Component<any,any> {
    render() {
	return ( <div {...this.props}>{this.props.children}</div> );
    }
}

export class ScrollView extends React.Component<any,any> {
    protected scrollRef: any;
    
    constructor(props: any) {
	super(props);
	this.scrollRef = React.createRef();
	this.state = {}
	//this.scrollTop = this.scrollTop.bind(this);
    }

    //@boundMethod
    get scrollTop(): number {
	if ( ! this.scrollRef.current ) return -1;
	return this.scrollRef.current.scrollTop;
    }

    render() {
	let style: React.CSSProperties = {...this.props.style};
	style = {...style, overflowY: 'scroll', overflowX: 'scroll' };

	return ( <div {...this.props} style={style} ref={this.scrollRef} >{ this.props.children }</div> );
    }
}
