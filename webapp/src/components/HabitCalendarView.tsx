import * as React from 'react';
import { View, ScrollView } from './platform/index';

import { boundMethod } from 'autobind-decorator'

import * as css from './HabitCalendarView.css';

import HabitCalendar, { HabitEntryBase, HabitEntry } from '../utils/HabitCalendar';

const scrollViewStyle = {
    position: 'absolute',
    top: '0px',
    bottom: '0px',
    left: '0px',
    right: '0px',
    overflow: 'scroll'
} as React.CSSProperties;


export interface HabitCalendarViewProps {
    habitCalendar: Readonly<HabitCalendar>,
    addHabitEntry(habitEntry: HabitEntryBase): void,
    setDateRange(startDate: Date, endDate: Date): void,
    style?: React.CSSProperties,
    className?: string
}
export interface HabitCalendarViewState {
    columnHeadersOffset: number
}

/*
// here just to remind how we can add some logic
const getInitialState = (props: HabitCalendarViewProps) => {
    rowOffset:    0,
    columnOffset: 0
};
*/

class RowView extends View {
    render() {
	let style: React.CSSProperties = {...this.props.style};
	style = {display: 'flex', flexDirection: 'row', ...style};
	return ( <div style={style}>{ this.props.children }</div> );
    }
}

class CellView extends View {
    render() {
	let style: React.CSSProperties = {...this.props.style};
	style = { flex: '1 1 0px', cursor: 'pointer', display: 'flex', justifyContent: 'center', alignItems: 'center', ...style};

	return ( <div style={style} onClick={this.props.onClick}>{ this.props.children }</div> );
    }
}
	
class DateIcon extends View {
    render() {
	let month = this.props.date.getMonth();
	
	let colors = ['#c0c0c0', '#a0a0a0'];
	let color = colors[month % colors.length];

	let style: React.CSSProperties = {...this.props.style};
	style = {
	    borderRadius: '50%',
	    backgroundColor: color,
	    width: '1cm',
	    height: '1cm',
	    margin: '1mm',
	    display: 'flex',
	    justifyContent: 'center',
	    alignItems: 'center',
	    color: 'white',
	    ...style
	};
	
	return ( <div style={style}>{ this.props.date.getDate() }</div> );
	}
}
class HabitIcon extends View {
    render() {
	let style: React.CSSProperties = {...this.props.style};
	style = {
	    display: 'flex',
	    justifyContent: 'center',
	    alignItems: 'center',
	    ...style
	};
	return ( <div style={style}>{this.props.id}</div> );
    }
}
class HabitEntryIcon extends View {
    render() {
	let value = this.props.entry.value;
	value = value ? value : 0;
	let colors = ['#c0c0c0', '#e04010', '#e0a020', '#80c000', '#408000'];
	let color = colors[value];

	let style: React.CSSProperties = {...this.props.style};
	style = {
	    borderRadius: '50%',
	    backgroundColor: color,
	    width: '1cm',
	    height: '1cm',
	    margin: '1mm',
	    ...style
	};
	
	return ( <div style={style}>{ }</div> );
    }
}


export class HabitCalendarView extends React.Component<HabitCalendarViewProps, HabitCalendarViewState> {
    readonly state: HabitCalendarViewState;

    protected gridRef: any;
    protected rowHeadersRef: any;
    protected columnHeadersRef: any;
    
    constructor(props: HabitCalendarViewProps) {
	super(props);
	this.gridRef = React.createRef();
	this.rowHeadersRef = React.createRef();
	this.columnHeadersRef = React.createRef();

	this.state = {
	    columnHeadersOffset: 0
	}

	this.onGridScroll = this.onGridScroll.bind(this);
    }

    componentDidMount() {
	//this.gridRef.addEventListener("onScroll", this.onGridScroll);
    }
    
    componentWillUnmount() {
	//this.gridRef.removeEventListener("onScroll", this.onGridScroll);
    }

    //@boundMethod
    onGridScroll(event: any) {
	//console.log(event);
	let top = this.gridRef.current.scrollTop;
	console.log(top);
	this.setState({columnHeadersOffset: top});
    }
    /*
    onColumnHeadersScroll(event: any) {

    }
    onRowHeadersScroll(event: any) {

    }
    */

    render() {
	let gridRows = [];
	let columnHeadersRow = [];
	let rowHeadersColumn = [];

	console.log('HabitCalendarView::render');
	//console.log(this.props);

	
	// column headers for fixed headers
	columnHeadersRow.push(<CellView key={'date'}></CellView>);
	for(let habit of this.props.habitCalendar.habits)
	    columnHeadersRow.push(<CellView key={habit.id}><HabitIcon id={habit.id} /></CellView>);
	// column headers for grid

	let columnHeadersStyle = {
	    position: 'absolute',
	    top: this.state.columnHeadersOffset + 'px',
	    left: '0px',
	    right: '0px',
	    background: 'rgba(255,255,255,0.8)',
	    height: '1cm'
	};
	gridRows.push(<RowView key={'column headers'} ref={this.columnHeadersRef} style={columnHeadersStyle}>{columnHeadersRow}</RowView>);

	// grid and row headers
	for(let day of this.props.habitCalendar.days) {
	    let rowKey = day.date.toISOString();
	    let rowCells = [];
	    let rowHeader = (
		    <CellView key={'date'}>
		      <DateIcon date={day.date} />
		    </CellView>
	    );
	    // row headers for fixed headers
	    rowHeadersColumn.push(<RowView key={rowKey}>{rowHeader}</RowView>);
	    // row header for grid
	    rowCells.push(rowHeader);

	    // cells
	    for(let habitEntry of day.habitEntryArray) {
		let cellKey = rowKey + '|' + habitEntry.habit.id + '|' + habitEntry.value;
		//console.log(cellKey);
		let addHabitEntry = this.props.addHabitEntry;
		let cell = (
			<CellView key={cellKey} onClick={
			    () => {
				let value = habitEntry.value ? habitEntry.value : 0;
				value = (value + 1) % 5;
				habitEntry = new HabitEntry(habitEntry.habit, habitEntry.date, value, new Date(Date.now()));
				//alert(JSON.stringify(habitEntry));
				addHabitEntry(habitEntry);
			    }
							}>
			<HabitEntryIcon entry={habitEntry} />
		    </CellView>
		)
		rowCells.push(cell);
	    }

	    gridRows.push(<RowView key={rowKey}>{rowCells}</RowView>);
	}

	

	let style = this.props.style ? this.props.style : {};
	let className = this.props.className ? this.props.className : "";

	return (
		<View style={style} className={className}>
		
		<ScrollView style={scrollViewStyle} ref={this.gridRef} onScroll={this.onGridScroll}>
		<View className={"calendar"}>
		{ gridRows }
		</View>
	        </ScrollView>
		{
		/*
		<ScrollView ref={this.rowHeadersRef} onScroll={this.onRowHeadersScroll}>
		{ rowHeadersColumn }
		</ScrollView>
		*/}

	        </View>
	)
    }
}



function hsl2rgb(h: number, s: number, l: number): Array<number> {
    let r, g, b;

    h = h / 360.;
    s = s / 100.;
    l = l / 100.;
    
    
    if (s == 0) {
        r = g = b = l; // achromatic
    } else {
        let hue2rgb = function hue2rgb(p: number, q: number, t: number) {
            if (t < 0) t += 1;
            if (t > 1) t -= 1;
            if (t < 1/6) return p + (q - p) * 6 * t;
            if (t < 1/2) return q;
            if (t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            return p;
        }

        let q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        let p = 2 * l - q;
        r = hue2rgb(p, q, h + 1/3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1/3);
    }

    return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
}
