import { Reducer } from 'redux';

import { SetDateRangeAction, SetActiveHabitsAction } from '../actions/HabitCalendarActions';

import { Habit } from '../utils/HabitCalendar';


export const initialHabits: Array<string> = [
    (new Habit('Sleep')).serialize(),
    (new Habit('Food')).serialize(),
    (new Habit('Sport')).serialize()
];

export const initialState: HabitCalendarState = {
      startDate: (new Date(Date.now() - 2*7*24*60*60*1000)).toISOString(),
      endDate: (new Date(Date.now())).toISOString(),
      activeHabits: initialHabits
};

interface HabitCalendarState {
       readonly startDate: string,
       readonly endDate: string,
       activeHabits: ReadonlyArray<string>
}


// Reducer
const reducer: Reducer<HabitCalendarState> = (state: HabitCalendarState = initialState, action: SetDateRangeAction | SetActiveHabitsAction ) => {
    switch (action.type) {
	case SetDateRangeAction.type: {
	    let payload = (action as SetDateRangeAction).payload;
	    return {
	    	...state,
		startDate: payload.startDate.toISOString(), // serialize
		endDate:   payload.endDate.toISOString(),     // serialize
	    };
	}
	case SetActiveHabitsAction.type: {
	    let payload = (action as SetActiveHabitsAction).payload;
	    return {
	    	...state,
		activeHabits: payload.activeHabits.map(habit => habit.serialize())
	    };
	}
	default:
	    return state;
    }
};

export default reducer;


// Selectors
export const selectors = {
    startDate:    (state: HabitCalendarState) => new Date(state.startDate), // deserialize
    endDate:      (state: HabitCalendarState) => new Date(state.endDate),   // deserialize
    activeHabits: (state: HabitCalendarState) => state.activeHabits.map(habit => Habit.deserialize(habit))
};

