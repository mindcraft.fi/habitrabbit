import { AddHabitEntryAction } from '../actions/HabitEntriesActions';

import { Habit, HabitEntry } from '../utils/HabitCalendar';

import { initialHabits } from './HabitCalendarReducer';

const testEntries: Array<string> = [
    (new HabitEntry(Habit.deserialize(initialHabits[0]), new Date(Date.now()), 1)).serialize(),
    (new HabitEntry(Habit.deserialize(initialHabits[1]), new Date(Date.now()-24*3600*1000), 2).serialize()),
    (new HabitEntry(Habit.deserialize(initialHabits[2]), new Date(Date.now()-2*24*3600*1000), 3).serialize())
];

// Initial state
export const initialState: HabitEntriesState = {
    habitEntries: testEntries, 
    isLoading: false,
    error: null
};

interface HabitEntriesState {
    habitEntries: ReadonlyArray<string>,
    isLoading: boolean,
    error: any
}


// Reducer
export default function reducer(state: HabitEntriesState = initialState, action: AddHabitEntryAction) {
    //console.log('HabitEntriesReducer');
    //console.log(state);
    //console.log(action);
    switch (action.type) {
	case AddHabitEntryAction.type:
	    let payload = (action as AddHabitEntryAction).payload;
	    return {
		...state,
		habitEntries: state.habitEntries.concat(payload.habitEntry.serialize())
	    };
	default:
	    return state;
    }
};

// Selectors
export const selectors = {
    habitEntries: (state: HabitEntriesState) => state.habitEntries.map(entry => HabitEntry.deserialize(entry))
};
