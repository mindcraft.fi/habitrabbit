import { combineReducers } from 'redux';
import { createSelector } from 'reselect';

import entriesReducer, { selectors as entriesSelectors, initialState as entriesInitialState } from './HabitEntriesReducer';

import calendarReducer, { selectors as calendarSelectors, initialState as calendarInitialState  } from './HabitCalendarReducer';

import HabitCalendar from '../utils/HabitCalendar';


export interface HabitRabbitState {
    habitEntries: any,
    habitCalendar: any
};

export const initialState: HabitRabbitState = {
    habitEntries: entriesInitialState,
    habitCalendar: calendarInitialState
};

// Reducers
// TODO: refactor calendar to include entries?
export const rootReducer = combineReducers({
    habitEntries:  entriesReducer,
    habitCalendar: calendarReducer
});


// Selectors
const habitCalendarSelector = createSelector(
    [
	(state: HabitRabbitState) => entriesSelectors.habitEntries(state.habitEntries),
	(state: HabitRabbitState) => calendarSelectors.activeHabits(state.habitCalendar),
	(state: HabitRabbitState) => calendarSelectors.startDate(state.habitCalendar),
	(state: HabitRabbitState) => calendarSelectors.endDate(state.habitCalendar),
    ],
    function createHabitCalendar(
	habitEntries,
	activeHabits,
	startDate,
	endDate
    ) {
	console.log(habitEntries);
	console.log(activeHabits);
	console.log(startDate);
	console.log(endDate);
	
	let habitCalendar: HabitCalendar = new HabitCalendar(activeHabits, startDate, endDate);
	for(let habitEntry of habitEntries)
	    habitCalendar.putOrIgnore(habitEntry);
	return habitCalendar;
    });


export const selectors = {
    habitCalendar: habitCalendarSelector,
    calendar: {
	startDate: (state: HabitRabbitState) => calendarSelectors.startDate(state.habitCalendar),
	endDate: (state: HabitRabbitState) => calendarSelectors.endDate(state.habitCalendar)
    }
};
