import * as React from 'react';
import * as ReactDOM from 'react-dom';
import HabitRabbitApp from './HabitRabbitApp';

ReactDOM.render(
    <HabitRabbitApp />,
    document.getElementById('root')
);
