import { HabitEntryBase } from '../utils/HabitCalendar';

export class AddHabitEntryAction {
    public static type: string = 'HabitEntries/ADD';
    public type: string;
    public payload: {
	habitEntry: HabitEntryBase
    }
    public constructor(habitEntry: HabitEntryBase) {
	this.type = AddHabitEntryAction.type;
	this.payload = {
	    habitEntry: habitEntry
	}
    }
    public static create(habitEntry: HabitEntryBase) {
	return {...(new AddHabitEntryAction(habitEntry))}
    }
}


export const actions = {
    AddHabitEntryAction
}
// Action creators
/*
export const actions = {
    add: (habitEntry: HabitEntryBase): AddHabitEntryAction => 
	new AddHabitEntryAction(habitEntry)
}
*/

