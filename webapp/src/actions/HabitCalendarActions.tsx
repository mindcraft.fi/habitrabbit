import { HabitBase } from '../utils/HabitCalendar';


export class SetDateRangeAction {
    public static type: string = 'HabitEntries/SET_DATE_RANGE';
    public type: string;
    public payload: {
	startDate: Date,
	endDate: Date	
    }
    public constructor(startDate: Date, endDate: Date) {
	this.type = SetDateRangeAction.type;
	this.payload = {
	    startDate: startDate,
	    endDate: endDate
	}
    }
    public static create(startDate: Date, endDate: Date) {
	return {...(new SetDateRangeAction(startDate, endDate))};
    }

}


export class SetActiveHabitsAction {
    public static type: string = 'HabitEntries/SET_ACTIVE_HABITS';
    public type: string;
    public payload: {
	activeHabits: ReadonlyArray<HabitBase>
    }
    public constructor(activeHabits: ReadonlyArray<HabitBase>) {
	this.type = SetActiveHabitsAction.type;
	this.payload = {
	    activeHabits: activeHabits
	}
    }
    public static create(activeHabits: ReadonlyArray<HabitBase>) {
	return {...(new SetActiveHabitsAction(activeHabits))};
    }
}

export const actions = {
    SetDateRangeAction,
    SetActiveHabitsAction
}
    

// Action creators
/*
export const actions = {
    setDateRange: (startDate: Date, endDate: Date): SetDateRangeAction => ({
	type: types.SET_DATE_RANGE,
	payload: {
	    startDate: startDate,
	    endDate: endDate
	}
    }),
    setActiveHabits: (habits: ReadonlyArray<HabitBase>): SetActiveHabitsAction => ({
	type: types.SET_ACTIVE_HABITS,
	payload: {
	    activeHabits: habits
	}
    })
};
*/
