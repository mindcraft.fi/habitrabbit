/**
 * Utility class for aggregating habit entries to a matrix for UI.
 * TODO: 
 * - Move classes Habit and HabitEntry to their own files 
 * - Export the main class
 * - Move/implement tests to their own file
 * - Add more documentation
 */

export const millisPerDay: number = 24 * 60 * 60 * 1000;

export interface HabitBase {
    readonly id: string,
    serialize(): string,
    //static deserialize(json: string): HabitBase
}

export class Habit implements HabitBase {
    protected _id: string;
    constructor(id: string) { this._id = id }
    get id(): string { return this._id }
    serialize(): string {
	return "" + this._id;
    }
    static deserialize(json: string): Habit {
	return new Habit(json);
    }
}

export interface HabitEntryBase {
    habit: HabitBase,
    date: Date,
    value: number | null,
    readonly timestamp: Date
    serialize(): string,
    //static deserialize(json: string): HabitEntryBase
}

export class HabitEntry implements HabitEntryBase {
    protected _timestamp: Date;
    constructor(
	public habit: HabitBase,
	public date: Date,
	public value: number,
	timestamp?: Date
    ) {
	this._timestamp = timestamp && timestamp || new Date(Date.now());
    }
    
    get timestamp(): Date {
	return this._timestamp;
    }

    serialize(): string {
	let json = {
	    habit: this.habit.serialize(),
	    date: this.date.toISOString(),
	    value: this.value,
	    timestamp: this._timestamp.toISOString()
	};
	return JSON.stringify(json);
    }
    
    static deserialize(jsonStr: string): HabitEntryBase {
	let json = JSON.parse(jsonStr);
	return new HabitEntry(
	    Habit.deserialize(json.habit),
	    new Date(json.date),
	    json.value,
	    new Date(json.timestamp)
	);
    }
}

export class HabitCalendarDay {
    constructor(public date: Date, public habitEntryArray: Array<HabitEntryBase>) {}
}    


export default class HabitCalendar {
    protected _startDate: Date;
    protected _endDate: Date;

    protected _habitArray: ReadonlyArray<HabitBase>;
    protected _dayArray: Array<HabitCalendarDay>;

    protected _habitIdToIndex: Map<string, number>; 
    protected _dateToIndex: Map<number, number>; 


    // TODO: habits is a reference => bad
    // could not find a way (keyword) to prevent changing the orignal array elements
    constructor(
	habits: ReadonlyArray<HabitBase>,
	startDate: Date,
	endDate: Date
    ) {
	this._startDate = new Date(startDate.valueOf());
	this._endDate = new Date(endDate.valueOf());
	this._habitArray = habits;
	this.build();
    }

    
    /**
     * get habit entry for the given habit on a given day
     * returns null if not found
     */
    get(habit: HabitBase, date: Date): HabitEntryBase | null {
	if ( ! this._habitIdToIndex.has(habit.id) )
	    return null;
	let habitIndex = this._habitIdToIndex.get(habit.id);

	let day: HabitCalendarDay | null = this.findDay(date);
	if ( day ) {
	    return day.habitEntryArray[habitIndex];
	}
	return null;
    }
    
    /**
     * put a habit entry on a given date to the calendar if it fits
     * overwrite the existing entry if the new entry has timestamp
     * that is (equal or) newer than the existing entry's timestamp
     */
    putOrIgnore(habitEntry: HabitEntryBase): void {
	// habit not included => don't insert
	if ( ! this._habitIdToIndex.has(habitEntry.habit.id) )
	    return;
	try {
	    if ( this.findDay(habitEntry.date) )
		 this.put(habitEntry);
	} catch(e) {}
    }
    
    /**
     * put a habit entry on a given date to the calendar
     * overwrite the existing entry if the new entry has timestamp
     * that is (equal or) newer than the existing entry's timestamp
     */
    put(habitEntry: HabitEntryBase): void {
	// habit not included => don't insert
	if ( ! this._habitIdToIndex.has(habitEntry.habit.id) )
	    throw "HabitCalendar::put: Invalid HabitEntry.Habit.id";
	
	let habitIndex: number = this._habitIdToIndex.get(habitEntry.habit.id);

	//console.log(habitEntry);
	let day: HabitCalendarDay | null = this.findDay(habitEntry.date);
	if ( day ) {
	    // if existing
	    if ( day.habitEntryArray[habitIndex] ) {
		// update only if newer
		if ( habitEntry.timestamp >= day.habitEntryArray[habitIndex].timestamp ) {
		    day.habitEntryArray[habitIndex] = habitEntry;
		}
		else {}
	    }
	    // does not exist
	    else {
		day.habitEntryArray[habitIndex] = habitEntry;
	    }
	}
	else
	    throw "HabitCalendar::put: HabitEntry.date is outside of caledar's date range";
    }

    get habits(): ReadonlyArray<HabitBase> {
	return this._habitArray;
    }
    get days(): ReadonlyArray<HabitCalendarDay> {
	return this._dayArray;
    }

    get startDate(): Readonly<Date> {
	return new Date(this._startDate);
    }
    get endDate(): Readonly<Date> {
	return new Date(this._endDate);
    }

    /*    
    set startDate(newStartDate: Date) {
	this._startDate = new Date(newStartDate.valueOf());
	this.build();
    }
    set endDate(newEndDate: Date) {
	this._endDate = new Date(newEndDate.valueOf());
	this.build();
    }

    setDateRange(newStartDate: Date, newEndDate: Date) {
	this._startDate = new Date(newStartDate.valueOf());
	this._endDate = new Date(newEndDate.valueOf());
	this.build();
    }
    */
    
    protected findDay(date: Date): HabitCalendarDay | null {
	// date not included => don't insert	
	if ( date.valueOf() < this._startDate.valueOf()
	     || date.valueOf() > this._endDate.valueOf() )
	    return null;

	let deltaTime: number = date.valueOf() - this._startDate.valueOf();
	let deltaDays: number = Math.floor(deltaTime / millisPerDay);

	//console.log(habitEntry);
	//console.log(deltaTime);
	//console.log(deltaDays);
	//console.log(habitIndex);
	let day = this._dayArray[deltaDays];
	if ( day )
	    return day;
	return null;
    }

    protected build() {
	// start date begins at 00:00:00.000
 	this._startDate.setHours(0);
	this._startDate.setMinutes(0);
	this._startDate.setSeconds(0);
	this._startDate.setMilliseconds(0);

	// end date ends at 23:59:59.999
	this._endDate.setHours(23);
	this._endDate.setMinutes(59);
	this._endDate.setSeconds(59);
	this._endDate.setMilliseconds(999);

	let date: Date = new Date(this._startDate.valueOf());
	let endDate: Date = new Date(this._endDate.valueOf());

	// build days
	this._dayArray = new Array<HabitCalendarDay>();
	while(date.valueOf() <= endDate.valueOf()) {
	    let day: HabitCalendarDay = new HabitCalendarDay(date, new Array<HabitEntryBase>());
	    // fill with nulls, null = entry does not exists
	    for(let i = 0; i < this._habitArray.length; ++i)
		day.habitEntryArray.push(new HabitEntry(this._habitArray[i], date, null, new Date(0)));
	    this._dayArray.push(day);
	    date = new Date(date.valueOf() + millisPerDay);
	}

	// build habit index
	this._habitIdToIndex = new Map<string, number>(); 
	for(let i = 0; i < this._habitArray.length; ++i)
	    this._habitIdToIndex.set(this._habitArray[i].id, i);
    }

    
    toString() : string {
	// for..of = value, for..in = key
	let str : string = "";

	// header
	str += "yyyy-MM-dd | ";
	for(let habit of this._habitArray) {
	    if ( habit )
		str += new String("        " + habit.id).slice(-8);
	    else
		str += "    -   ";
	}

	str += "\n";

	// rows
	for(let day of this._dayArray) {
	    // hacky way for fixed length strings coming...
	   
	    str += ( day.date.getFullYear()
		     + "-" + new String( "00" + (day.date.getMonth()+1) ).slice(-2)
		     + "-" + new String( "00" + day.date.getDate() ).slice(-2) );
	    str += " | ";

	    //console.log(day.habitEntryArray);

	    for(let habitEntry of day.habitEntryArray) {
		if ( habitEntry && habitEntry.value )
		    str += "    " + habitEntry.value + "   ";
		else
		    str += "    •   ";
	    }
	    
	    str += "\n";
	}
	
	str += "\n";
	
	return str;
    }
}

