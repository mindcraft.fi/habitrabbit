import HabitCalendar, { Habit, HabitBase, HabitEntry, millisPerDay } from './HabitCalendar';

describe('HabitCalendar', () => {
    test('create, put and get', () => {
	let habits: Array<Habit> = new Array<Habit>();
	habits.push(new Habit("test #1"));
	habits.push(new Habit("test #2"));
	habits.push(new Habit("test #3"));
	let anotherHabit = new Habit("test #4");

	let endDate: Date = new Date(2019,1-1,1,10,0,0,0);
	let startDate: Date = new Date(endDate.valueOf() - 2 * 7 * millisPerDay);

	
	let habitCalendar: HabitCalendar = new HabitCalendar(habits, startDate, endDate);

	let testEntry: HabitEntry = null;

	testEntry = new HabitEntry(habits[0], new Date(endDate.valueOf() - 1 * millisPerDay), 1);
	habitCalendar.putOrIgnore(testEntry);
	expect(habitCalendar.get(testEntry.habit, testEntry.date)).toBe(testEntry);

	testEntry = new HabitEntry(habits[0], new Date(endDate.valueOf() - 1 * millisPerDay), 1);
	habitCalendar.putOrIgnore(testEntry);
	expect(habitCalendar.get(testEntry.habit, testEntry.date)).toBe(testEntry);

	testEntry = new HabitEntry(habits[1], new Date(endDate.valueOf() + 1 * millisPerDay), 2);
	habitCalendar.putOrIgnore(testEntry);
	expect(habitCalendar.get(testEntry.habit, testEntry.date)).toBe(null);

	testEntry = new HabitEntry(habits[2], new Date(endDate.valueOf() - 2 * millisPerDay), 3);
	habitCalendar.putOrIgnore(testEntry);
	expect(habitCalendar.get(testEntry.habit, testEntry.date)).toBe(testEntry);

	testEntry = new HabitEntry(habits[0], new Date(endDate.valueOf() - 1 * millisPerDay), 4);
	habitCalendar.putOrIgnore(testEntry);
	expect(habitCalendar.get(testEntry.habit, testEntry.date)).toBe(testEntry);

	testEntry = new HabitEntry(habits[1], new Date(endDate.valueOf() + 0.001 * millisPerDay), 5);
	habitCalendar.putOrIgnore(testEntry);
	expect(habitCalendar.get(testEntry.habit, testEntry.date)).toBe(testEntry);

	testEntry = new HabitEntry(habits[0], new Date(endDate.valueOf() - 13 * millisPerDay), 6);
	habitCalendar.putOrIgnore(testEntry);
	expect(habitCalendar.get(testEntry.habit, testEntry.date)).toBe(testEntry);

	testEntry = new HabitEntry(habits[2], new Date(endDate.valueOf() - 14 * millisPerDay), 7);
	habitCalendar.putOrIgnore(testEntry);
	expect(habitCalendar.get(testEntry.habit, testEntry.date)).toBe(testEntry);

	testEntry = new HabitEntry(habits[1], new Date(endDate.valueOf() - 15 * millisPerDay), 8);
	habitCalendar.putOrIgnore(testEntry);
	expect(habitCalendar.get(testEntry.habit, testEntry.date)).toBe(null);

	testEntry = new HabitEntry(anotherHabit, new Date(endDate.valueOf() - 7 * millisPerDay), 9);
	habitCalendar.putOrIgnore(testEntry);
	expect(habitCalendar.get(testEntry.habit, testEntry.date)).toBe(null);

	
	testEntry = new HabitEntry(habits[1], new Date(endDate.valueOf() - 6.5 * millisPerDay), 1);
	habitCalendar.putOrIgnore(testEntry);
	expect(habitCalendar.get(testEntry.habit, testEntry.date)).toBe(testEntry);

	testEntry = new HabitEntry(habits[1], new Date(endDate.valueOf() - 6.1 * millisPerDay), 2);
	habitCalendar.putOrIgnore(testEntry);
	expect(habitCalendar.get(testEntry.habit, testEntry.date)).toBe(testEntry);


	try {
	    testEntry = new HabitEntry(anotherHabit, new Date(2019, 1-1, 2, 0), 10);
	    habitCalendar.put(testEntry);
	    throw "Should throw error";
	}
	catch(e) {}

	
	console.log(habitCalendar.toString());

	//console.log(habitCalendar.get(habits[0], new Date(endDate.valueOf() - 14 * millisPerDay)));
	//console.log(habitCalendar.get(habits[0], new Date(endDate.valueOf() - 13 * millisPerDay)));
	//console.log(habitCalendar.get(habits[0], new Date(endDate.valueOf() - 12 * millisPerDay)));

	expect(habitCalendar.get(habits[0], new Date(endDate.valueOf() - 14 * millisPerDay)).value).toBeNull();
	expect(habitCalendar.get(habits[0], new Date(endDate.valueOf() - 13 * millisPerDay)).value).not.toBeNull();
	expect(habitCalendar.get(habits[0], new Date(endDate.valueOf() - 12 * millisPerDay)).value).toBeNull();

	expect(habitCalendar.get(habits[1], new Date(endDate.valueOf() - 14 * millisPerDay)).value).toBeNull();
	expect(habitCalendar.get(habits[1], new Date(endDate.valueOf() - 13 * millisPerDay)).value).toBeNull();
	expect(habitCalendar.get(habits[1], new Date(endDate.valueOf() - 12 * millisPerDay)).value).toBeNull();

	expect(habitCalendar.get(habits[2], new Date(endDate.valueOf() - 14 * millisPerDay)).value).not.toBeNull();
	expect(habitCalendar.get(habits[2], new Date(endDate.valueOf() - 13 * millisPerDay)).value).toBeNull();
	expect(habitCalendar.get(habits[2], new Date(endDate.valueOf() - 12 * millisPerDay)).value).toBeNull();


    })
});





